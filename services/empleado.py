import datetime
from datetime import datetime

from fastapi import HTTPException
from starlette import status
from models.empleado import Empleado as EmpleadoModel
from schemas.empleado import Empleado


class EmpleadoService:
    def __init__(self, db) -> None:
        self.db = db

    def get_empleados(self):
        result = self.db.query(EmpleadoModel).all()
        return result

    def get_empleado(self, id):
        result = self.db.query(EmpleadoModel).filter(EmpleadoModel.id == id).first()
        return result

    def get_empleados_by_identificacion(self, identificacion):
        result = self.db.query(EmpleadoModel).filter(EmpleadoModel.numero_identificacion == identificacion).all()
        return result

    def create_empleado(self, empleado: Empleado):
        new_empleado = EmpleadoModel(**empleado.dict())

        si_existe = self.existe_empleado(new_empleado)
        if si_existe.__len__() > 0:
            raise HTTPException(status_code=status.HTTP_422_UNPROCESSABLE_ENTITY, detail="Este Usuario ya esta creado")

        fecha, mensaje = self.es_fecha_valida(new_empleado)
        if fecha == False:
            raise HTTPException(status_code=status.HTTP_422_UNPROCESSABLE_ENTITY, detail=mensaje)

        correo_generado = self.obtener_correo_electronico(new_empleado)
        new_empleado.correo_electronico = correo_generado

        self.db.add(new_empleado)
        self.db.commit()
        return

    def obtener_correo_electronico(self, new_empleado):
        """
        Metodo ObtenerCorreoElectronico

        Metodo que genera automaticamente el correo electronico del empleado y valida si existe previamente,
        en caso dado genera un consecutivo
        :param new_empleado:
        :return:
        """
        dominio = ".us"
        if new_empleado.pais == "Colombia":
            dominio = ".co"

        nombres_apellidos = new_empleado.primer_nombre.lower() + new_empleado.primer_apellido.replace(" ", "").lower()

        correo_generado = nombres_apellidos + "@" + "cidenet.com" + dominio

        result = self.db.query(EmpleadoModel).filter(EmpleadoModel.correo_electronico == correo_generado).all()
        if result.__len__() > 0:
            switch = True
            indicador = 0
            while switch == True:
                indicador += 1
                correo_generado = nombres_apellidos + str(indicador) + "@" + "cidenet.com" + dominio
                result = self.db.query(EmpleadoModel).filter(EmpleadoModel.correo_electronico == correo_generado).all()
                if result.__len__() == 0:
                    switch = False
        return correo_generado

    def es_fecha_valida(self, new_empleado):
        """
        Metodo EsFechaValida

        Este metodo nos arroja si la fecha es valida o no y el detalle
        :param new_empleado:
        :return: fecha_valida(False si no es valida), mensaje(detalle del error en la fecha)
        """
        hoy = datetime.now()
        fecha_actual = hoy.date()
        diferencia_dias = fecha_actual - new_empleado.fecha_ingreso

        fecha_valida = True
        mensaje = ""
        if new_empleado.fecha_ingreso > fecha_actual:
            mensaje = "la fecha es mayor a la actual"
            fecha_valida = False
        elif diferencia_dias.days > 30:
            mensaje = "la fecha es menor en mas de un mes a la fecha actual"
            fecha_valida = False

        return fecha_valida, mensaje

    def existe_empleado(self, new_empleado):
        """
        Metodo ExisteEmpleado

        Metodo que valida si ya existe el empleado en nuestra base de datos
        :param new_empleado:
        :return: si_existe: valor entero correspondiente a la cantidad de empleados que existe
        """
        si_existe = self.db.query(EmpleadoModel).filter(
            EmpleadoModel.tipo_identificacion == new_empleado.tipo_identificacion,
            EmpleadoModel.numero_identificacion == new_empleado.numero_identificacion
        ).all()

        return si_existe

    def update_empleado(self, id: int, data: Empleado):
        empleado = self.db.query(EmpleadoModel).filter(EmpleadoModel.id == id).first()
        empleado.primer_apellido = data.primer_apellido
        empleado.segundo_apellido = data.segundo_apellido
        empleado.primer_nombre = data.primer_nombre
        empleado.otros_nombres = data.otros_nombres
        empleado.fecha_ingreso = data.fecha_ingreso
        empleado.pais = data.pais
        empleado.tipo_identificacion = data.tipo_identificacion
        empleado.numero_identificacion = data.numero_identificacion
        empleado.area = data.area
        empleado.fecha_edicion = datetime.now()

        fecha, mensaje = self.es_fecha_valida(empleado)
        if fecha == False:
            raise HTTPException(status_code=status.HTTP_422_UNPROCESSABLE_ENTITY, detail=mensaje)

        if empleado.primer_nombre != EmpleadoModel.primer_nombre or EmpleadoModel.primer_apellido != data.primer_apellido:
            correo_generado = self.obtener_correo_electronico(empleado)
            empleado.correo_electronico = correo_generado

        self.db.commit()
        return

    def delete_empleado(self, id: int):
        self.db.query(EmpleadoModel).filter(EmpleadoModel.id == id).delete()
        self.db.commit()
        return
