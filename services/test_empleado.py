from fastapi.testclient import TestClient

from main import app

client_app = TestClient(app)

def test_create_empleado_sucesfull() -> None:
    create_empleado = {
        "primer_apellido": "PEREZ",
        "segundo_apellido": "ORTIZ",
        "primer_nombre": "JORGE",
        "otros_nombres": "LUIS",
        "pais": "Colombia",
        "tipo_identificacion": "Cédula de Ciudadanía",
        "numero_identificacion": "COL1047437474",
        "area": "Financiera",
        "fecha_ingreso": "2023-01-03"
    }

    response = client_app.post(url='/empleados', json=create_empleado)

    assert response.status_code == 201

    assert response.json() == {'message': 'Se ha registrado el empleado'}

def test_create_empleado_error_empleado_ya_existe() -> None:
    create_empleado = {
        "primer_apellido": "PEREZ",
        "segundo_apellido": "ORTIZ",
        "primer_nombre": "JORGE",
        "otros_nombres": "LUIS",
        "pais": "Colombia",
        "tipo_identificacion": "Cédula de Ciudadanía",
        "numero_identificacion": "COL1047437474",
        "area": "Financiera",
        "fecha_ingreso": "2023-01-03"
    }

    response = client_app.post(url='/empleados', json=create_empleado)

    assert response.status_code == 422

    assert response.json() == {'message': 'Este Usuario ya esta creado'}